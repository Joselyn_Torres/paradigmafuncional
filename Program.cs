﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Paradigmas
{
    public  class Program 
    {
        static void Main(string[] args)
        {
            // Paradigma Funcional

            var numero = Contador();
            Console.WriteLine("El primer valor de la función es: " + numero.funcion());
            numero.incrementar();                              // Inicia en 10, pero se incrementa + 1
            Console.WriteLine("Incrementamos +1: "+ numero.funcion());
            numero.incrementar();                             // Se incrementa +1
            Console.WriteLine("Incrementamos +1: "+ numero.funcion());
            numero.incrementar();                             // Se incrementa +1
            Console.WriteLine("Incrementamos +1: " + numero.funcion());
            numero.disminuir();                               // Disminuimos -1
            Console.WriteLine("Disminuimos -1: "+ numero.funcion());    //Obtención del resultado
        }
        public static (Action incrementar, Action disminuir, Func<int> funcion) Contador()
        {
            int valor = 10;
            Action incrementar = () => valor++;
            Action disminuir = () => valor--;
            Func<int> funcion = () => valor;

            return (incrementar, disminuir, funcion);
        }
    }
}
 